# simple-shop-ci-cd-aws

## Solution description

### Technology stack
- API: Python + Flask + Mongo
    - `api.py` contains the REST API application
    - `apiHelper/helper.py` small helper class
- UI: Vue.js
    - files served by flask server:
        - `static` folder contains the template HTML file
        - `js/app.js` javascript Vue code
        - `js/env.js` for setting the hostname of REST API - created dynamically by docker-compose (local/docker/prod)
- UI Tests: Python + Behave + Selenium + Docker => running on Gitlab CI 
    - `test/features/`
        - `basic_shop.feature` gherkin BDD steps for UI test
        - `steps/steps.py` steps description
        - `steps/pages/shop.py` shop page object with all elements and actions
    - reports are avaible through Gitlab CI in Job artifacts, example report: https://jaroslavburi.gitlab.io/-/simple-shop-ci-cd-aws/-/jobs/135806860/artifacts/logs/allure-report-firefox/index.html
- Cloud: AWS EC2 - deployed by Gitlab CI after successful tests through ssh, with usage of treescale.com docker registry
    - deployment described in `.gitlab-ci.yml` in task `deploy` 

### Running the solution
- local development:
    - requirements:
        - python3 with packages `flask-restful pymongo requests` 
        - docker or mongo instance
    - requires mongo instance - use provided docker from docker-compose `docker run`
        - `docker-compose -f docker-compose-local.yml up mongo`
        - or custom `docker run mongo ...` 
        - mongo instance is expected at `http://mongo:27017`
    - server run: `python3 api.py` - editable at `api.py`
    - server is running on http://localhost:5000
    - run init script to populate products with `python3 init_mongo.py`

- local development with docker
    - easy to run, but a bit slower turnaround after code changes (need to kill and rerun docker-compose)
    - requirements: only docker
    - run server + mongo + init script `docker-compose -f docker-compose-local.yml up --build`
    - server is running on http://localhost:5000

- running UI tests against local deployment
    - requirements: 
        - python3 with packages `behave selenium requests`
        - chromedriver - install using brew on Mac or download from http://chromedriver.chromium.org/downloads
            - in `test/features/environment.py` set the valid path for chromedriver executable
    - from root folder run `behave test/features`
    
- running full app with UI tests (for CI)
    - requirements: just docker
    - run `docker-compose up --build`
    
- after commit to Gitlab automatic pipeline is run, if it succeeds it deploys the solution to AWS. All the variables are in Gitlab settings
 
## Improvements for the future
- common environment.config for all services, currently the env setting is not very consistent
- authentication/authorization/users
- REST API
    - input validation

- UI
    - better request handling - after change is made to DB, change the app programatically, until the new state is recovered from DB

- Tests
    - ...
    
- Cloud deployment
    - Go full AWS - Use native docker management AWS ECS, together with AWS docker registry ECR, deploy through AWS API
        - benefits: integrated Healthcheck, load balancer, ...


