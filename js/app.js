var basePath = "http://" + window.host + ":5000"
console.log(basePath)
new Vue({
  el: '#app',
  data() {
    return {
      products: null,
      cart: null,
      orders: null,
      orderMessage: null,
      selectedOrder: null,
      selectedOrderProducts: null
    }
  },
  mounted() {
    this.getProducts()
    this.getCart()
    this.getOrders()
  },
  methods: {
    getProducts: function () {
      axios
        .get(basePath + '/products')
        .then(response => (this.products = response.data))
    },

    getCart: function () {
      axios
        .get(basePath + '/cart')
        .then(response => { this.cart = response.data })
    },

    getOrders: function () {
      axios
        .get(basePath +  '/orders')
        .then(response => { this.orders = response.data })
    },

    addToCart: function (name) {
      axios
        .post(basePath +  '/cart', {
          product: name
        })

      setTimeout(() => { this.getCart() }, baseTime)
    },

    deleteFromCart: function (name) {
      axios
        .delete(basePath + '/cart', {
          params: { product: name }
        })

      setTimeout(() => { this.getCart() }, baseTime)
    },

    submitOrder: function (name) {
      if (this.cart.length > 0) {
        this.createOrder()
        setTimeout(() => { this.deleteCart() }, baseTime * 2)
        setTimeout(() => { this.getCart() }, baseTime * 3)
        setTimeout(() => { this.getOrders() }, baseTime * 3)
      }
    },

    deleteCart: function () {
      axios.delete(basePath + '/emptyCart')
    },

    createOrder: function () {
      var id = makeid();
      var currentCart = "";

      var first = true;
      for (var item of this.cart) {
        (first) ? first = false : currentCart += ",";
        currentCart += item.name
      }

      axios
        .post(basePath + '/order/' + id, {
          products: currentCart
        })

      this.orderMessage = "Created order " + id;
    },

    deleteOrder: function (id) {
      if (getDeleteOrderConfirmation(id)) {
        axios.delete(basePath + '/order/' + id)
        setTimeout(() => { this.getOrders() }, 500)
      }
    },

    viewOrder: function (id) {
      this.selectedOrder = "Order " + id
      axios
        .get(basePath + '/order/' + id)
        .then(response => { this.selectedOrderProducts = parseToArray(response.data) })
    },
  },
})

var baseTime = 500;

function makeid() {
  var text = "";
  var possible = "0123456789";

  for (var i = 0; i < 10; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

function getDeleteOrderConfirmation(id) {
  var retVal = confirm("Are you sure you want to delete order " + id);
  return retVal;
}

function parseToArray(data) {
  return data.products.split(",");
}
