from behave import given, when, then
from pages.shop import Shop
import requests
import time

@given("clean cart and orders")
def step_impl(context):
    requests.delete(context.homepage + 'emptyCart')
    requests.delete(context.homepage + 'emptyOrders')


@when("I add product {product} to cart")
def step_impl(context, product):
    Shop.clickAddtoCart(context, product)


@when("I remove product {product} from cart")
def step_impl(context, product):
    Shop.clickRemoveFromCart(context, product)


@when("I submit order")
def step_impl(context):
    Shop.clickSubmitOrder(context)


# Verifies success message after ordering
# Sets context.order to newly created order
@then(u'new order is created and cart is empty')
def step_impl(context):
    elem = Shop.getElementByTestData(context, "orderMessage")

    search_text = "Created order "
    assert elem.text.__contains__(search_text)
    context.order = elem.text[len(search_text):]
    assert Shop.wait_for_list_size(context, "cart", 0)


@when(u'I view the order')
# Requires context.order set
def step_impl(context):
    Shop.clickViewOrder(context, context.order)


@when(u'I delete the order')
# Requires context.order set
def step_impl(context):
    Shop.deleteOrder(context, context.order)


@then(u'I have {number} products in cart')
def step_impl(context, number):
    assert Shop.wait_for_list_size(context, "cart", int(number))


@then(u'I have {number} orders in orders')
def step_impl(context, number):
    assert Shop.wait_for_list_size(context, "order", int(number))
