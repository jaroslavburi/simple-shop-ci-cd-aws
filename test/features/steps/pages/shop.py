import time

class Shop:

    @staticmethod
    def getElementByTestData(context, element):
        return context.driver.find_element_by_css_selector(
        f"[test_data='{element}']")

    @staticmethod
    def clickAddtoCart(context, product):
        elem = Shop.getElementByTestData(context, f'addToCart{product}')
        elem.click()

    @staticmethod
    def clickRemoveFromCart(context, product):
        elem = Shop.getElementByTestData(context, f'removeFromCart{product}')
        elem.click()

    @staticmethod
    def clickSubmitOrder(context):
        elem = Shop.getElementByTestData(context, 'submitOrder')
        elem.click()

    @staticmethod
    def clickViewOrder(context, order):
        elem = Shop.getElementByTestData(context, f'viewOrder{order}')
        elem.click()

    @staticmethod
    def deleteOrder(context, order):
        elem = context.driver.find_element_by_css_selector(
        f"[test_data='deleteOrder{order}']")
        elem.click()

        confirm = context.driver.switch_to.alert
        confirm.accept()

    #Waits until `list` has specified `number` of elements
    @staticmethod
    def wait_for_list_size(context, list, number, tries=200):
        context.driver.implicitly_wait(0) #dont wait if the element is not found

        returnVal = False
        for i in range(tries):
            elem = context.driver.find_elements_by_class_name(list)
            if(len(elem) == number):
                returnVal = True
                break
            time.sleep(0.01)

        context.driver.implicitly_wait(context.default_implicit_wait)

        return returnVal

