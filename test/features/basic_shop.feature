Feature: Basic shop

    Background: Clean
        Given clean cart and orders

    Scenario: Order product
        When I add product Shoes to cart
        And I add product Car to cart
        And I remove product Shoes from cart
        And I add product House to cart
        Then I have 2 products in cart
        When I submit order
        Then new order is created and cart is empty

    Scenario: Delete order
        When I add product Shoes to cart
        Then I have 1 products in cart
        When I submit order
        Then new order is created and cart is empty
        When I view the order
        And I delete the order
        Then I have 0 orders in orders
