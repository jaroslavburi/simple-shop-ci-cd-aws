from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from os import environ

def before_all(context):

    driver = None
    if environ.get('ENV') == "docker":
        driver = webdriver.Remote(
            command_executor='http://chrome:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.CHROME)
        context.homepage = "http://app:5000/"
    else:
        driver = webdriver.Chrome('/usr/local/chromedriver-Darwin')
        context.homepage = "http://127.0.0.1:5000/"

    context.default_implicit_wait = 10
    driver.implicitly_wait(context.default_implicit_wait)
    context.driver = driver
    


def before_feature(context, feature):
    context.driver.get(context.homepage)


def after_all(context):
    context.driver.close()
