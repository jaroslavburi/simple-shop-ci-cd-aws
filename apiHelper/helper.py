class Helper:

    #variable - requested variable to return from request
    #reqparse - flasks implementation of request handling
    #return - returns desired object from request
    @staticmethod
    def getVariableFromReq(variable, reqparse):
        parser = reqparse.RequestParser()
        parser.add_argument(variable)
        args = parser.parse_args()
        return args[variable]
