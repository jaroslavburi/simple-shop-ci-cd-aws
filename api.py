from flask import Flask, jsonify, send_from_directory
from flask_restful import Api, Resource, reqparse
import pymongo
from apiHelper.helper import Helper
from bson import json_util

mongo_client = pymongo.MongoClient("mongodb://mongo:27017/")
db = mongo_client["shop"]

app = Flask(__name__)
api = Api(app)


class Product(Resource):
    def get(self, name):
        product = db.products.find_one({"name": name}, {"_id": 0})
        return jsonify(product)

    def post(self, name):
        product = db.products.find_one({"name": name}, {"_id": 0})
        if product is None:
            db.products.insert_one({
                "name": name
            })

    def delete(self, name):
        db.products.delete_one({
            "name": name,
        })


class Products(Resource):
    def get(self):
        products = db.products.find({}, {"_id": 0})
        doc = []
        for product in products:
            doc.append(product)

        return jsonify(doc)


class Cart(Resource):
    def get(self):
        cart = db.cart.find({}, {"_id": 0})
        doc = []
        for product in cart:
            doc.append(product)

        return jsonify(doc)

    def post(self):
        product_name = Helper.getVariableFromReq("product", reqparse)
        product = db.products.find_one({"name": product_name}, {"_id": 0})

        if product is not None:
            db.cart.insert_one({
                "name": product_name,
            })

    def delete(self):
        product_name = Helper.getVariableFromReq("product", reqparse)

        db.cart.delete_one({
            "name": product_name,
        })


class EmptyCart(Resource):
    def delete(self):
        db.cart.delete_many({})


class Order(Resource):
    def get(self, id):
        order = db.orders.find_one({"id": id}, {"_id": 0})
        return jsonify(order)

    def post(self, id):
        products = Helper.getVariableFromReq("products", reqparse)

        if products != "":
            db.orders.insert_one({
                "id": id,
                "products": products,
            })

    def delete(self, id):
        print(id)
        db.orders.delete_one({"id": id})


class Orders(Resource):
    def get(self):
        orders = db.orders.find({}, {"_id": 0})
        doc = []
        for order in orders:
            doc.append(order)

        return jsonify(doc)


class EmptyOrders(Resource):
    def delete(self):
        db.orders.delete_many({})


api.add_resource(Product, "/product/<string:name>")
api.add_resource(Products, "/products")

api.add_resource(Cart, "/cart")
api.add_resource(EmptyCart, "/emptyCart")

api.add_resource(Order, "/order/<string:id>")
api.add_resource(Orders, "/orders")
api.add_resource(EmptyOrders, "/emptyOrders")


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('js', path)


@app.route('/favicon.ico')
def send_ico():
    return app.send_static_file('favicon.ico')


@app.route('/')
def root():
    return app.send_static_file('index.html')


app.run(debug=True, host='0.0.0.0')
