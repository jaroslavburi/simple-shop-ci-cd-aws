import requests
from os import environ

host = "127.0.0.1"
if environ.get('ENV') == "aws":
    host = "ec2-3-17-14-177.us-east-2.compute.amazonaws.com"

requests.post(f'http://{host}:5000/product/Shoes')
requests.post(f'http://{host}:5000/product/Car')
requests.post(f'http://{host}:5000/product/House')
